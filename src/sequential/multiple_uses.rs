//! A runtime which allows executing nodes multiple times using reference-counted activators.
//!
//! WARNING: This runtime is implemented using dynamic reference counting.  This makes it somewhat
//! dubious for its intended purpose of being able to reuse nodes through dependency cycles, as it
//! prevents the memory from ever being de-allocated.  This can be fixed relatively easily by
//! replacing the `Rc` implementations with a custom version using references instead; however,
//! this requires to statically allocate the inner structures as well and would cause a fair amount
//! of boring bookkeeping.  Since this is out of scope for the project, we'll accept the memory
//! leaks; but if you are interested, you can try to make this implementation leak-free.  You can
//! use an [arena](https://docs.rs/typed-arena/1.4.1/typed_arena/struct.Arena.html) to store the
//! data in a buffer, and may also be interested in using the `RefPort` from the `common` module
//! instead of `RcPort`.

use api::prelude::*;
use common::prelude::*;

use std::cell::{Cell, RefCell, RefMut};
use std::rc::Rc;

use sequential::port::RcPort;

/// The inner structure for the iterator.  This include a handle to the node, as well as a pending
/// count with interior mutability.  Contrary to the `single_use` implementation, we also use
/// interior mutability for the handle because we need to be able to access the handle while there
/// are still other references to the inner structure (hence the reusable nature).
#[derive(Debug)]
struct RcActivatorInner<H: ?Sized> {
    /// The pending count.  If 0, there is currently a builder or a handle pointing to the node.
    pending: Cell<usize>,
    /// The initial pending count to reset to.  This includes the handle.
    initial: Cell<usize>,
    /// The underlying node to schedule.
    handle: RefCell<H>,
}

impl<H> RcActivatorInner<H> {
    fn new(node: H) -> Self {
        RcActivatorInner {
            pending: Cell::new(0),
            initial: Cell::new(1),
            handle: RefCell::new(node),
        }
    }
}

impl<H: ?Sized> RcActivatorInner<H> {
    /// Rearm the activation structure with a new pending count. This should only be called when
    /// the activator was depleted.
    fn rearm(&self) {
        assert!(self.pending.get() == 0);
        self.pending.set(self.initial.get());
    }

    /// Decrement the pending count and return the new pending count.
    fn decrement_pending(&self) -> usize {
        let old_pending = self.pending.get();
        assert!(old_pending > 0);

        let pending = old_pending - 1;
        self.pending.set(pending);
        pending
    }
}

/// A reference-counted, reusable activator.
///
/// The activator contains a handle to a node, a counter for the number of activations
/// remaining before the node should be scheduled, and the total number of activators.
///
/// When the node is finalized, the counter is set to the total number of activators.  It is
/// decremented by one on each activation, and the node is scheduled when the counter reaches zero.
#[derive(Debug)]
pub struct RcActivator<H: ?Sized> {
    inner: Rc<RcActivatorInner<H>>,
}

/// A default activator which schedules a panicking node.  This can be used as a placeholder
/// activator when the target node is not yet known.  Note that trying to activate this will
/// already trigger a panic in `decrement_pending` since it never gets armed.
impl<'r> Default for RcActivator<RuntimeNode<'r>> {
    fn default() -> Self {
        RcActivator {
            inner: Rc::new(RcActivatorInner::new(UninitializedNode)),
        }
    }
}

impl<'r> ActivatorOnce<Runtime<'r>> for RcActivator<RuntimeNode<'r>> {
    fn activate_once(self, scheduler: &mut Runtime<'r>) {
        if self.inner.decrement_pending() == 0 {
            scheduler.schedule(RcHandle { inner: self.inner })
        }
    }
}

impl<'r> ActivatorMut<Runtime<'r>> for RcActivator<RuntimeNode<'r>> {
    fn activate_mut(&mut self, scheduler: &mut Runtime<'r>) {
        Activator::activate(self, scheduler)
    }
}

impl<'r> Activator<Runtime<'r>> for RcActivator<RuntimeNode<'r>> {
    fn activate(&self, scheduler: &mut Runtime<'r>) {
        if self.inner.decrement_pending() == 0 {
            scheduler.schedule(RcHandle {
                inner: self.inner.clone(),
            })
        }
    }
}

/// A node handle.  This is the structured used to actually schedule nodes.  A single handle to a
/// given node should ever exist, and it can only exist when the node's pending count is 0.
#[derive(Debug)]
pub struct RcHandle<H: ?Sized> {
    inner: Rc<RcActivatorInner<H>>,
}

impl<S, H: NodeMut<S> + ?Sized> NodeOnce<S> for RcHandle<H>
where
    RcActivator<H>: ActivatorOnce<S>,
{
    /// Execute the guard.  This consumes the guard and re-arm the activators, which allows the
    /// node to be executed again later.
    fn execute_once(self, scheduler: &mut S) {
        self.inner.rearm();
        self.inner.handle.borrow_mut().execute_mut(scheduler);
        RcActivator { inner: self.inner }.activate_once(scheduler);
    }
}

/// A builder for reusable nodes.  Allow creation of activators and arms them when finalized.
#[derive(Debug)]
pub struct RcBuilder<N> {
    inner: Rc<RcActivatorInner<N>>,
}

impl<N> RcBuilder<N> {
    fn new(node: N) -> Self {
        RcBuilder {
            inner: Rc::new(RcActivatorInner::new(node)),
        }
    }
}

impl<'r, N: NodeMut<Runtime<'r>> + 'r> NodeBuilder<Runtime<'r>> for RcBuilder<N> {
    type Node = N;

    fn add_activator(&mut self) -> RcActivator<RuntimeNode<'r>> {
        self.inner.initial.set(self.inner.initial.get() + 1);

        RcActivator {
            inner: self.inner.clone(),
        }
    }

    fn finalize(&mut self, _runtime: &mut Runtime<'r>) {
        self.inner.rearm();
        self.inner.decrement_pending();
    }
}

impl<'a, 'r: 'a, N: NodeMut<Runtime<'r>> + 'r> NodeBorrowMut<'a, Runtime<'r>> for RcBuilder<N> {
    type RefMut = RefMut<'a, N>;

    fn borrow_mut(&'a mut self) -> Self::RefMut {
        self.inner.handle.borrow_mut()
    }
}

/// The type of nodes manipulated by the sequential reusable runtime.
pub type RuntimeNode<'r> = dyn NodeMut<Runtime<'r>> + 'r;

pub type RuntimeActivator<'r> = RcActivator<RuntimeNode<'r>>;

/// A sequential runtime for reusable graphs.
pub struct Runtime<'r> {
    ready: Vec<RcHandle<RuntimeNode<'r>>>,
}

impl<'r> Runtime<'r> {
    /// Create a new sequential runtime for reusable graphs.
    pub fn new() -> Self {
        Runtime { ready: Vec::new() }
    }

    /// Run all the nodes until all work is completed.
    pub fn execute(&mut self) {
        while let Some(guard) = self.ready.pop() {
            guard.execute_once(self)
        }
    }
}

impl<'r> Scheduler for Runtime<'r> {
    type Handle = RcHandle<RuntimeNode<'r>>;

    fn schedule(&mut self, handle: Self::Handle) {
        self.ready.push(handle);
    }
}

impl<'r> GraphSpec for Runtime<'r> {
    type Activator = RuntimeActivator<'r>;
}

impl<'r, N: NodeMut<Runtime<'r>> + 'r> NodeSpec<N> for Runtime<'r> {
    type Builder = RcBuilder<N>;

    fn node(&self, node: N) -> Self::Builder {
        RcBuilder::new(node)
    }
}

impl<'r, T: Default + 'r> PortSpec<T> for Runtime<'r> {
    type Port = RcPort<Cell<T>>;

    fn port(&self, init: T) -> Self::Port {
        RcPort::new(Cell::new(init))
    }
}
