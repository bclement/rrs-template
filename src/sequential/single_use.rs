//! Sequential implementation of a single-use runtime with reference-counted activators.

use std::cell::Cell;
use std::marker::PhantomData;
use std::rc::Rc;

use api::prelude::*;

use sequential::port::RcPort;

/// The inner structure for a single-use activator, containing the pending count and the node
/// handle.
struct RcActivatorInner<'r> {
    /// The pending count.
    pending: Cell<usize>,

    /// The underlying node to schedule.  Note that we store a Box of a trait object here, instead
    /// of using a type parameter and embedding the node in the structure.  This is because of a
    /// Rust limitation which prevents us from calling a method with `self` as argument on a trait
    /// object -- the same reason why we use `NodeBox`.  Unfortunately, that trick only works for
    /// `Box`, which the Rust compiler has special knowledge of -- so instead we use an extra level
    /// of indirection and put a box here.
    handle: Box<RuntimeNode<'r>>,
}

impl<'r> RcActivatorInner<'r> {
    fn new<N: NodeBox<Runtime<'r>> + 'r>(node: N) -> Self {
        RcActivatorInner {
            pending: Cell::new(0),
            handle: Box::new(node),
        }
    }
}

/// A reference-counted, single-use activator.
///
/// The activator contains a handle to a node, as well as a counter for the number of activations
/// remaining before the node should be scheduled.
///
/// When the node is finalized, the counter is set to the number of activators created.  It is
/// decremented by one on each activation, and the node is scheduled when the counter reaches zero.
/// Since activating consumes an activator, we ensure that the pending count only ever reaches zero
/// if all activators have been called.
pub struct RcActivator<'r> {
    inner: Rc<RcActivatorInner<'r>>,
}

impl<'r> ActivatorOnce<Runtime<'r>> for RcActivator<'r> {
    fn activate_once(self, scheduler: &mut Runtime<'r>) {
        let pending = self.inner.pending.get();
        assert!(pending > 0);

        if pending == 1 {
            scheduler.schedule(Rc::try_unwrap(self.inner).ok().unwrap().handle)
        } else {
            self.inner.pending.set(pending - 1);
        }
    }
}

/// A builder for single-use nodes.  Allow creation of activators and arms them when finalized.
///
/// Note that once the builder is created, no modifications to the node are permitted (the builder
/// does not implement the `NodeBorrowMut` trait).  This is due to the fact that we need to store a
/// (boxed) trait object inside the activator in order to be able to call it later using
/// `execute_box`; see the documentation on `RcActivatorInner`.
pub struct RcBuilder<'r, N> {
    inner: Rc<RcActivatorInner<'r>>,
    _marker: PhantomData<*const N>,
    num_activators: usize,
}

impl<'r, N: NodeBox<Runtime<'r>> + 'r> RcBuilder<'r, N> {
    fn new(node: N) -> Self {
        RcBuilder {
            inner: Rc::new(RcActivatorInner::new(node)),
            _marker: PhantomData,
            num_activators: 0,
        }
    }
}

impl<'r, N: NodeBox<Runtime<'r>> + 'r> NodeBuilder<Runtime<'r>> for RcBuilder<'r, N> {
    type Node = N;
    fn add_activator(&mut self) -> RcActivator<'r> {
        self.num_activators += 1;

        RcActivator {
            inner: self.inner.clone(),
        }
    }
    fn finalize(&mut self, _runtime: &mut Runtime<'r>) {
        self.inner.pending.set(self.num_activators);
    }
}

/// The type of nodes manipulated by the sequential single-use runtime.
type RuntimeNode<'r> = dyn NodeBox<Runtime<'r>> + 'r;

/// A sequential runtime for single-use graphs.
pub struct Runtime<'r> {
    /// The list of nodes which are ready to execute.  We use a dynamic trait object to store any
    /// type of node which can be executed once.  Note that a similar parallel implementation would
    /// store `dyn NodeBox<Runtime<'r>> + Send + 'r` types instead.
    ready: Vec<Box<RuntimeNode<'r>>>,
}

impl<'r> Runtime<'r> {
    /// Creates a new sequential runtime for single-use graphs.
    pub fn new() -> Self {
        Runtime { ready: Vec::new() }
    }

    /// Runs all the nodes until all work is completed.
    pub fn execute(&mut self) {
        while let Some(node) = self.ready.pop() {
            node.execute_box(self);
        }
    }
}

impl<'r> Scheduler for Runtime<'r> {
    type Handle = Box<RuntimeNode<'r>>;

    fn schedule(&mut self, handle: Self::Handle) {
        self.ready.push(handle);
    }
}

impl<'r> GraphSpec for Runtime<'r> {
    type Activator = RcActivator<'r>;
}

impl<'r, N: NodeBox<Runtime<'r>> + 'r> NodeSpec<N> for Runtime<'r> {
    type Builder = RcBuilder<'r, N>;

    fn node(&self, node: N) -> Self::Builder {
        RcBuilder::new(node)
    }
}

impl<'r, T: Default + 'r> PortSpec<T> for Runtime<'r> {
    type Port = RcPort<Cell<T>>;

    fn port(&self, init: T) -> Self::Port {
        RcPort::new(Cell::new(init))
    }
}
